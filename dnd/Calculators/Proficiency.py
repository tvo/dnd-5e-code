import math

from dnd.Loaders.IPlayerCharacter import IPlayerCharacter


class ProficiencyCalculator:
    @staticmethod
    def calculate_proficiency(player: IPlayerCharacter) -> int:
        (character, my_class, race) = player.get_entities()
        level = character.mainClass.level
        for c in character.multiClasses:
            level += c.level
        return int(math.floor(level))
