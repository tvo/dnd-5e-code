from dnd.Loaders.IPlayerCharacter import IPlayerCharacter


class HealthPointCalculator:
    @staticmethod
    def max_health(player: IPlayerCharacter) -> int:
        (character, my_class, race) = player.get_entities()
        stats = player.get_stats()
        return my_class.calculate_max_hp(stats, character.mainClass.level, True)
