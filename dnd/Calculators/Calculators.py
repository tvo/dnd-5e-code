from dataclasses import dataclass
from easy_inject import ServiceCollection

from dnd.Calculators.WeaponCharacter import WeaponCharacter
from dnd.Calculators.HealthPointCalculator import HealthPointCalculator
from dnd.Calculators.Proficiency import ProficiencyCalculator


@dataclass
class Calculators:
    proficiency: ProficiencyCalculator
    health: HealthPointCalculator
    weapon_character: WeaponCharacter

    @staticmethod
    def dependencies(services: ServiceCollection):
        services.register(ProficiencyCalculator)
        services.register(HealthPointCalculator)
        services.register(Calculators).singleton()
        services.register(WeaponCharacter).singleton()
