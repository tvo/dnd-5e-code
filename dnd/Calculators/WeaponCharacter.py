import math
from dataclasses import replace

from dnd.Entity.DiceRoll import DiceRoll
from dnd.Entity.Weapon import Weapon
from dnd.Loaders.IPlayerCharacter import IPlayerCharacter


class WeaponCharacter:
    @staticmethod
    def decorate_weapon(weapon: Weapon, player: IPlayerCharacter) -> Weapon:
        (character, my_class, race) = player.get_entities()
        if len(weapon.damage_roll.dice) == 0:
            return weapon
        stats = player.get_stats()
        roll: DiceRoll = weapon.damage_roll

        # finesse or not to finesse
        if weapon.finesse and stats.dexterity >= stats.strength:
            roll = DiceRoll.parse(roll.__str__() + ' + dex')
        else:
            roll = DiceRoll.parse(roll.__str__() + ' + str')

        # am I proficient with the weapon?
        if weapon.category in my_class.weapon_proficiencies:
            p = player.get_proficiency()
            roll = replace(roll, plus=(roll.plus + p))
            weapon = replace(weapon, to_hit_bonus=(weapon.to_hit_bonus + p))

        return replace(weapon, damage_roll=roll)

