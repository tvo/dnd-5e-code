from dataclasses import dataclass
from typing import List

from dnd.Entity.Feature import Feature
from dnd.Loaders.IPlayerCharacter import IPlayerCharacter
from dnd.Loaders.Loaders import ClassLoader, FeatureLoader


@dataclass
class FeatureCalculator:
    class_loader: ClassLoader
    feature_loader: FeatureLoader

    def all_features(self, player: IPlayerCharacter) -> List[Feature]:
        (character, my_class, race) = player.get_entities()
        feature_names = []
        feature_names.extend(race.features)
        feature_names.extend(my_class.features)
        for class_name in character.multiClasses:
            c = self.class_loader.fetch(class_name)
            feature_names.extend(c.features)

        feature_names = set(feature_names)
        features = []
        for f in feature_names:
            features.append(self.feature_loader.fetch(f))
        return features
