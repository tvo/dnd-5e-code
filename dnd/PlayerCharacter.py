import math
from typing import Optional, Any, List, Iterator, Tuple

from rich.prompt import Prompt

from dnd.Calculators.Calculators import Calculators
from dnd.Entity.Character import Character
from dnd.Entity.CharacterClass import CharacterClass
from dnd.Entity.Choices.Choice import Choice
from dnd.Entity.Class import Class
from dnd.Entity.Feature import Feature
from dnd.Entity.Race import Race
from dnd.Entity.Stat import Stat
from dnd.Loaders.IPlayerCharacter import IPlayerCharacter
from dnd.Loaders.Loaders import Loaders


class PlayerCharacter(IPlayerCharacter):
    def __init__(self, loaders: Loaders, calculators: Calculators):
        self.loaders = loaders
        self.calc = calculators
        self.character: Optional[Character] = None
        self.race: Optional[Race] = None
        self.my_class: Optional[Class] = None
        self.features: List[Feature] = []
        self.stats: Stat = Stat()
        self.hp: int = 0
        self.max_hp: int = 0

    def get_entities(self) -> Tuple[Character, Class, Race]:
        return self.character, self.my_class, self.race

    def get_stats(self) -> Stat:
        return self.stats

    def get_proficiency(self) -> int:
        return int(math.ceil(self.character.mainClass.level / 4)) + 1

    @staticmethod
    def enumerate_choices_for_entity(entity: Any) -> Iterator[Choice]:
        if isinstance(entity, Choice):
            yield entity
        else:
            for name, value in vars(entity).items():
                if isinstance(value, Choice):
                    yield value
                elif isinstance(value, List):
                    for value_item in value:
                        for r in PlayerCharacter.enumerate_choices_for_entity(value_item):
                            yield r

    def load_character(self, name: str) -> Optional[Character]:
        self.character = self.loaders.character_loader.fetch(name)
        if self.character is None:
            return None
        self.initialize_character_properties()
        return self.character

    def initialize_character_properties(self):
        self.race = self.loaders.race_loader.fetch(self.character.race_name)
        self.my_class = self.loaders.class_loader.fetch(self.character.mainClass.class_name)

        # setup features
        feature_names = []
        feature_names.extend(self.my_class.features)
        feature_names.extend(self.race.features)
        self.features = []
        for f in feature_names:
            self.features.append(self.loaders.feature_loader.fetch(f))

        # setup stats
        self.stats = self.character.base_stats
        self.stats = self.stats.plus(self.race.statBonus)
        for choice in self.character.choices:
            # noinspection PyUnresolvedReferences
            increase = choice.stat_increase
            if isinstance(increase, Stat):
                self.stats = self.stats.plus(increase)
        # todo: stat bonuses from level up

        self.max_hp = self.calc.health.max_health(self)
        self.hp = self.max_hp

    def ask(self, entity: Any) -> Iterator[Choice]:
        for choice in self.enumerate_choices_for_entity(entity):
            choice.ask_unanswered()
            yield choice

    def setup_character(self) -> Character:
        if self.character is not None:
            return self.character
        choices: List[Choice] = []
        all_races = self.loaders.race_loader.load()
        all_classes = self.loaders.class_loader.load()

        name = Prompt.ask(prompt='Name?')
        # race
        race_name = Prompt.ask(prompt='Race', choices=list(map(lambda r: r.name, all_races)))
        race = self.loaders.race_loader.fetch(race_name)
        choices.extend(list(self.ask(race)))

        # class
        class_name = Prompt.ask(prompt='Class', choices=list(map(lambda b: b.name, all_classes)))
        my_class = self.loaders.class_loader.fetch(class_name)
        choices.extend(self.ask(my_class))

        class_level = int(Prompt.ask(prompt='class level'))

        # prep result
        character_class = CharacterClass(class_name=class_name, level=class_level, level_up_choices=[])
        self.character = Character(name=name, mainClass=character_class, race_name=race_name)

        self.character.choices = choices
        self.initialize_character_properties()
        return self.character

    def load(self, name: str):
        character = self.loaders.character_loader.fetch(identifier=name)
        self.character = character
        if character is not None:
            self.initialize_character_properties()

    def save(self, character: Optional[Character] = None):
        if character is None:
            character = self.character
        self.loaders.character_loader.save(character)
