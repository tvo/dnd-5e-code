from __future__ import annotations

import math
from dataclasses import dataclass, replace
from typing import List, Callable, Iterator, Union, Optional


stat_abbreviation = ['con', 'str', 'dex', 'int', 'wis', 'cha']


@dataclass()
class Stat:
    constitution: int = 0
    strength: int = 0
    dexterity: int = 0
    intelligence: int = 0
    wisdom: int = 0
    charisma: int = 0

    def operate(self, other: Stat, f: Callable[[int, int], int]) -> Stat:
        zipped = zip(self.to_array(), other.to_array())
        return Stat.from_array(list(map(lambda z: f(z[0], z[1]), zipped)))

    def to_array(self) -> List[int]:
        return [self.constitution, self.strength, self.dexterity, self.intelligence, self.wisdom, self.charisma]

    @staticmethod
    def from_array(stat_list: Union[List[int], Iterator[int]]) -> Stat:
        if not isinstance(stat_list, List):
            stat_list = list(stat_list)
        assert len(stat_list) == 6, 'list must have a length of 6.'
        return Stat(
            constitution=stat_list[0],
            strength=stat_list[1],
            dexterity=stat_list[2],
            intelligence=stat_list[3],
            wisdom=stat_list[4],
            charisma=stat_list[5]
        )

    @staticmethod
    def _to_mod_func(number: int) -> int:
        return int(math.floor((number - 10) / 2.0))

    def to_modifier(self) -> Stat:
        return self.from_array(map(lambda x: self._to_mod_func(x), self.to_array()))

    def get_stat_value(self, key: str) -> Optional[int]:
        key = key.lower()
        full = ['constitution', 'strength', 'dexterity', 'intelligence', 'wisdom', 'charisma']
        abv = ['con', 'str', 'dex', 'int', 'wis', 'cha']
        arr = self.to_array()
        for i, s in enumerate(full):
            if s in key:
                return arr[i]
        for i, s in enumerate(abv):
            if s in key:
                return arr[i]
        return None

    def plus(self, other: Stat) -> Stat:
        return self.operate(other, lambda a, b: a + b)

    @staticmethod
    def get_from_lose_label(label: str, amount: Optional[int]) -> Stat:
        amount = amount or 1
        stat_key = Stat.from_array([0, 1, 2, 3, 4, 5])
        stat_value = stat_key.get_stat_value(label)
        stat_array = [0, 0, 0, 0, 0, 0]
        stat_array[stat_value] = amount
        return Stat.from_array(stat_array)
