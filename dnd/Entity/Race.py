from dataclasses import dataclass, field
from typing import List

from dnd.Entity.Choices.Choice import Choice
from dnd.Entity.Feature import Feature
from dnd.Entity.Stat import Stat


@dataclass
class Race:
    id: str
    name: str
    statBonus: Stat = field(default_factory=lambda: Stat(0, 0, 0, 0, 0, 0))
    features: List[Feature] = field(default_factory=lambda: [])
    resistances: List[str] = field(default_factory=lambda: [])
    bonus_stat_choices: List[Choice] = field(default_factory=lambda: [])
    size: str = 'medium'
    darkVision: bool = False
    movement: int = 30
    movementFly: int = 0

