from dataclasses import dataclass


@dataclass()
class Equipment:
    id: str
    name: str
    description: str = ''
    value: float = 0
    weight: float = 0
