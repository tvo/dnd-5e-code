from __future__ import annotations
from dataclasses import dataclass, field
from typing import List, Tuple

from dnd.Entity.Choices.Choice import Choice
from dnd.Entity.Stat import Stat


@dataclass
class CharacterClass:
    level: int = 0
    multiclass: bool = False
    class_name: str = ''
    race_name: str = ''
    base_stats: Stat = field(default_factory=lambda: Stat())
    level_up_choices: List[Tuple[int, Choice]] = field(default_factory=lambda: [])
