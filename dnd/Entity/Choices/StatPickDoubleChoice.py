from dataclasses import dataclass
from dnd.Entity.Choices.StatPickChoice import StatPickChoice


@dataclass
class StatPickDoubleChoice(StatPickChoice):
    def ask_unanswered(self):
        self.message = 'Pick a stat to increase by 2.'
        super().ask_unanswered()
        self.stat_increase = self.stat_increase.plus(self.stat_increase)
