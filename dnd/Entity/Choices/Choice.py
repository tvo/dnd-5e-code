from abc import ABC, abstractmethod
from dataclasses import field
from typing import List


class Choice (ABC):
    id: str = 'id'
    tags: List[str] = field(default_factory=lambda: [])

    @abstractmethod
    def ask_unanswered(self):
        pass
