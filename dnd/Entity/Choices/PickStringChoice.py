from dataclasses import field, dataclass
from typing import List

from rich.prompt import Prompt

from dnd.Entity.Choices.Choice import Choice


@dataclass
class PickStringChoice(Choice):
    answer: str = None
    choices: List[str] = field(default_factory=lambda: [])

    def ask_unanswered(self):
        self.answer = Prompt.ask("Choose one of the following", choices=self.choices)
