from dataclasses import dataclass
from typing import List

from rich.prompt import Prompt

from dnd.Entity.Choices.Choice import Choice


@dataclass
class FeatChoice(Choice):
    feat_id_answer: str
    available_feats: List[str]

    def ask_unanswered(self):
        self.feat_id_answer = Prompt.ask("Choose a feat", choices=self.available_feats)
