from dataclasses import dataclass

from rich.prompt import Prompt

from dnd.Entity.Choices.Choice import Choice
from dnd.Entity.Stat import Stat


@dataclass
class StatPickChoice(Choice):
    stat_increase: Stat = None
    message: str = 'Pick a stat to increase.'

    def ask_unanswered(self):
        up = Prompt.ask(self.message, choices=['con', 'str', 'dex', 'int', 'wis', 'cha'])
        stat = Stat()
        if up == 'con':
            stat.constitution = 1
        elif up == 'str':
            stat.strength = 1
        elif up == 'dex':
            stat.dexterity = 1
        elif up == 'int':
            stat.intelligence = 1
        elif up == 'wis':
            stat.wisdom = 1
        elif up == 'cha':
            stat.charisma = 1
        self.stat_increase = stat

