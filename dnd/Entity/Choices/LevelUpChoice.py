from dataclasses import dataclass

from dnd.Entity.Choices.Choice import Choice
from dnd.Entity.Stat import Stat


@dataclass
class LevelUpChoice(Choice):
    # either pick a feature
    feature_pick: str = None
    # or two ability scores
    stat_increase: Stat = None

    def ask_unanswered(self):
        pass
