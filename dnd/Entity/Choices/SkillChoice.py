from dataclasses import dataclass
from dnd.Entity.Choices.PickStringChoice import PickStringChoice


@dataclass
class SkillChoice(PickStringChoice):
    pass
