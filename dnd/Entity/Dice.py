import random
from dataclasses import dataclass


@dataclass
class Dice:
    sides: int

    def roll(self) -> int:
        return random.randint(1, self.sides)

    def expected_value(self) -> float:
        return self.sides / 2 + 0.5

    def __str__(self):
        return f'1d{self.sides}'
