from dataclasses import dataclass, field
from typing import List, Optional


@dataclass()
class PointCounter:
    name: str
    value: int
    max: Optional[int]
    reset: List[str] = field(default_factory=lambda: ['long rest'])
