from __future__ import annotations

import math
from dataclasses import dataclass, field
from typing import List, Optional, Tuple
from typing import TYPE_CHECKING

from dnd.Entity.Choices.Choice import Choice
from dnd.Entity.Feature import Feature
from dnd.Entity.Stat import Stat

if TYPE_CHECKING:
    from dnd.Entity.DiceRoll import DiceRoll


def _ceil(v: float) -> int:
    return int(math.ceil(v))


@dataclass()
class Class:
    name: str
    hit_dice: DiceRoll
    hit_start: DiceRoll
    spell_casting_ability: Optional[str]
    level_up_choices: List[Tuple[int, Choice]] = field(default_factory=lambda: [])
    subclasses: List[Class] = field(default_factory=lambda: [])
    features: List[Feature] = field(default_factory=lambda: [])
    weapon_proficiencies: List[str] = field(default_factory=lambda: [])
    armor_proficiencies: List[str] = field(default_factory=lambda: [])
    skill_proficiencies: List[str] = field(default_factory=lambda: [])
    tools_proficiencies: List[str] = field(default_factory=lambda: [])
    saving_throw_proficiencies: Stat = field(default_factory=lambda: Stat())
    resistances: List[str] = field(default_factory=lambda: [])

    def calculate_max_hp(self, stat: Stat, level: int, do_lvl_one: bool) -> int:
        stat = stat.to_modifier()
        max_hp = 0
        if do_lvl_one:
            max_hp = _ceil(self.hit_start.expected_value() + stat.constitution)
        for _ in range(1, level):
            max_hp += _ceil(self.hit_dice.expected_value() + stat.constitution)
        return max_hp
