from typing import Generic, TypeVar, Optional

T = TypeVar('T')


class State(Generic[T]):
    def __init__(self):
        self.Value: Optional[T] = None
