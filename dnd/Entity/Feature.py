from dataclasses import dataclass, field
from typing import List


@dataclass()
class Feature:
    id: str
    name: str
    description: str
    class_requirement: List[str] = field(default_factory=lambda: [])
    prerequisites: List[str] = field(default_factory=lambda: [])
    level: int = 0

