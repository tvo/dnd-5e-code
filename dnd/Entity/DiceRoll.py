from __future__ import annotations

import math
from dataclasses import dataclass, field
from itertools import groupby
from typing import List, Optional

import d20

from dnd.Entity.Dice import Dice
from d20 import roll, Expression, Die, Literal

from dnd.Entity.Stat import Stat, stat_abbreviation


@dataclass
class DiceRoll:
    dice: List[Dice] = field(default_factory=lambda: [])
    plus: int = 0
    stat_multiplier_add: Stat = field(default_factory=lambda: Stat())

    @property
    def stat(self) -> Stat:
        return self.stat_multiplier_add

    def _stat_additions(self, character_stats: Optional[Stat]) -> float:
        if character_stats is None:
            return 0
        character_stats = character_stats.to_modifier() or Stat()
        return sum(self.stat.operate(character_stats, lambda a, b: a * b).to_array())

    def roll(self, character_stats: Optional[Stat] = None) -> int:
        value = self.plus + self._stat_additions(character_stats)
        d: Dice
        for d in self.dice:
            value += d.roll()
        return int(math.floor(value))

    def expected_value(self, character_stats: Optional[Stat] = None) -> float:
        value = self.plus + self._stat_additions(character_stats)
        d: Dice
        for d in self.dice:
            value += d.expected_value()
        return value

    @staticmethod
    def parse(raw: str) -> DiceRoll:
        if (raw or '') == '':
            return DiceRoll(dice=[])

        stat = Stat.from_array(map(lambda x: raw.count(x), stat_abbreviation))
        for attr in stat_abbreviation:
            # replace the text with a 0, because + 0 evaluates to 0 for the d20 parser.
            raw = raw.replace(attr, '0')

        r = roll(raw)
        dice = []
        plus = []

        def traverse(ex: Optional[Expression]):
            if ex is None:
                return
            elif isinstance(ex, Die):
                dice.append(Dice(sides=ex.size))
                return
            elif isinstance(ex, Literal):
                plus.append(ex.total)
                return
            elif isinstance(ex, d20.Dice):
                for d in ex.values:
                    traverse(d)
                return
            if hasattr(ex, 'children'):
                for e in ex.children:
                    traverse(e)
                return
            else:
                if hasattr(ex, 'left'):
                    traverse(ex.left)
                if hasattr(ex, 'right'):
                    traverse(ex.right)

        traverse(r.expr)

        return DiceRoll(dice=dice, plus=sum(plus), stat_multiplier_add=stat)

    def __str__(self):
        dice = []
        dice.extend(self.dice)

        def projection(d: Dice):
            return d.sides

        dice.sort(key=projection)
        x_grouped = [list(it) for k, it in groupby(dice, projection)]
        d_str = []
        for group in x_grouped:
            d_str.append(f'{len(group)}d{group[0].sides}')
        result = ' + '.join(d_str)
        if self.plus != 0:
            if self.plus > 0:
                result += f' + {self.plus}'
            else:
                result += f' - {self.plus * -1}'
        for times, name in zip(self.stat.to_array(), stat_abbreviation):
            for _ in range(0, times):
                result += f' + {name}'
        return result.strip()
