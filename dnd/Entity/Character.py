from dataclasses import dataclass, field
from typing import List, Optional

from dnd.Entity.CharacterClass import CharacterClass
from dnd.Entity.Choices.Choice import Choice
from dnd.Entity.Stat import Stat


@dataclass
class Character:
    id: str = ''
    name: str = ''
    race_name: str = ''
    mainClass: CharacterClass = field(default_factory=lambda: CharacterClass())
    base_stats: Stat = field(default_factory=lambda: Stat(10, 10, 10, 10, 10, 10))
    subClass: Optional[CharacterClass] = field(default_factory=lambda: [])
    multiClasses: List[CharacterClass] = field(default_factory=lambda: [])
    choices: List[Choice] = field(default_factory=lambda: [])
    movementOverride: int = None
    movementFlyOverride: int = None
    movementSwimOverride: int = None

    # @property
    # def movement_walking(self) -> int:
    #     if self.movementOverride is not None:
    #         return self.movementOverride
    #     return self.race.movement
    #
    # @property
    # def movement_fly(self) -> int:
    #     if self.movementFlyOverride is not None:
    #         return self.movementFlyOverride
    #     return self.race.movementFly
    #
    # @property
    # def movement_swim(self) -> int:
    #     if self.movementSwimOverride is not None:
    #         return self.movementSwimOverride
    #     return int(self.movement_walking / 2.0)

    # @property
    # def features(self) -> List[Feature]:
    #     feature = List[Feature]()
    #     feature.extend(self.mainClass.features)
    #     if self.subClass is not None:
    #         feature.extend(self.subClass.features)
    #     # for c in self.multiClasses:
    #     #     feature.extend(c.)
    #     for c in self.multiSubClasses:
    #         feature.extend(c.features)
    #     feature.extend(self.race.features)
    #     return feature
