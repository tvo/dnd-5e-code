import random
from dataclasses import dataclass, field

from dnd.Entity.DiceRoll import DiceRoll
from dnd.Entity.Equipment import Equipment
from dnd.Entity.Stat import Stat


@dataclass
class Weapon (Equipment):
    damage_roll: DiceRoll = field(default_factory=lambda: DiceRoll(dice=[]))
    to_hit_bonus: int = 0
    range: int = 5
    light: bool = False
    two_handed: bool = False
    finesse: bool = False
    loading: bool = False
    versatile: bool = False
    thrown: bool = False
    reach: bool = False
    heavy: bool = False
    category: str = ''

    @property
    def to_hit(self) -> int:
        return random.randint(1, 20) + self.to_hit_bonus

    @property
    def to_hit_expected_value(self) -> float:
        return 10.5 + self.to_hit_bonus

    def damage(self, character_stats: Stat) -> int:
        return self.damage_roll.roll(character_stats)

    def damage_expected_value(self, character_stats: Stat) -> float:
        return self.damage_roll.expected_value(character_stats)
