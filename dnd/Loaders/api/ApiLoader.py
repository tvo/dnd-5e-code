from easy_inject import ServiceCollection
from dnd.Entity.Character import Character
from dnd.Entity.Class import Class
from dnd.Entity.Equipment import Equipment
from dnd.Entity.Feature import Feature
from dnd.Entity.Race import Race
from dnd.Entity.Weapon import Weapon
from dnd.Loaders.ILoader import ILoader
from dnd.Loaders.ILoaderCollection import ILoaderCollection
from dnd.Loaders.api.ApiSettings import ApiSettings
from dnd.Loaders.api.CharacterApiLoader import CharacterApiLoader
from dnd.Loaders.api.ClassApiLoader import ClassApiLoader
from dnd.Loaders.api.EquipmentApiLoader import EquipmentApiLoader
from dnd.Loaders.api.FeatureApiLoader import FeatureApiLoader
from dnd.Loaders.api.RaceApiLoader import RaceApiLoader
from dnd.Loaders.api.WeaponApiLoader import WeaponApiLoader


class ApiLoaders (ILoaderCollection):
    def __init__(self,
                 weapon: WeaponApiLoader,
                 class_: ClassApiLoader,
                 equipment: EquipmentApiLoader,
                 race: RaceApiLoader,
                 feature: FeatureApiLoader,
                 character: CharacterApiLoader,
                 ):
        self.weapon_loader: WeaponApiLoader = weapon
        self.class_loader: ClassApiLoader = class_
        self.character_loader: CharacterApiLoader = character
        self.equipment_loader: EquipmentApiLoader = equipment
        self.race_loader: RaceApiLoader = race
        self.feature_loader: FeatureApiLoader = feature

    def get_weapon(self) -> ILoader[Weapon]:
        return self.weapon_loader

    def get_equipment(self) -> ILoader[Equipment]:
        return self.equipment_loader

    def get_class(self) -> ILoader[Class]:
        return self.class_loader

    def get_race(self) -> ILoader[Race]:
        return self.race_loader

    def get_character(self) -> ILoader[Character]:
        return self.character_loader

    def get_feature(self) -> ILoader[Feature]:
        return self.feature_loader

    @staticmethod
    def dependencies(services: ServiceCollection):
        services.register(ApiSettings).As(ApiSettings).singleton()
        services.register(WeaponApiLoader).As(WeaponApiLoader).singleton()
        services.register(RaceApiLoader).singleton()
        services.register(ClassApiLoader).singleton()
        services.register(CharacterApiLoader).singleton()
        services.register(EquipmentApiLoader).singleton()
        services.register(FeatureApiLoader).singleton()
        services.register(ApiLoaders).As(ApiLoaders).singleton()
