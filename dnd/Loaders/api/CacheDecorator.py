import json
import pathlib


def persist_to_file(file_name):
    def decorator(original_func):
        path = pathlib.Path().resolve().__str__() + '/data/cache/' + file_name
        try:
            cache = json.load(open(path, 'r'))
        except (IOError, ValueError):
            cache = {}

        def new_func(param):
            if param not in cache:
                cache[param] = original_func(param)
                json.dump(cache, open(path, 'w'))
            return cache[param]
        return new_func

    return decorator
