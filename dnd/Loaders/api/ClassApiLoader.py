from typing import Union, List, Any, Optional

import requests

from dnd.Entity.Class import Class
from dnd.Loaders.ILoader import ILoader, T


class ClassApiLoader (ILoader[Class]):
    def load(self) -> List[T]:
        data = requests.get('https://www.dnd5eapi.co/api/equipment-categories/adventuring-gear')
        response: List[Class] = []
        for e in data['equipment']:
            eq = Class(name=e['name'])
            response.append(eq)
        return response

    def fetch(self, identifier: Any) -> Optional[T]:
        pass

    def save(self, items: Union[List[T], T]):
        pass

    def save_one(self, item: T):
        pass

