import json
from typing import Union, List, Any, Optional

import requests

from dnd.Entity.Dice import Dice
from dnd.Entity.DiceRoll import DiceRoll
from dnd.Entity.Weapon import Weapon
from dnd.Loaders.api.ApiSettings import ApiSettings


class WeaponApiLoader:
    def __init__(self, settings: ApiSettings):
        self.settings = settings
        self.url = settings.url + '/api/weapon'

    def load(self) -> List[Weapon]:
        response = requests.get(self.url)
        data = response.json()
        result: List[Weapon] = []
        for e in data:
            e['damage_roll'] = DiceRoll.parse(e['damage_roll'])
            result.append(Weapon(**e))
        return result

    def fetch(self, identifier: str) -> Optional[Weapon]:
        pass

    def save(self, items: Union[List[Weapon], Weapon]):
        if items is None:
            return
        if isinstance(items, Weapon):
            self.save_one(items)
        else:
            for item in items:
                self.save_one(item)

    def save_one(self, item: Weapon):
        if item is None:
            return
        params = item.__dict__.copy()
        dto = Weapon(**params)
        d = dto.__dict__
        d.pop('id')
        d['damage_roll'] = item.damage_roll.__str__()
        data = json.dumps(d)
        response = requests.put(url=self.url + f'/{item.id}', data=data)
        assert response.status_code == 200
