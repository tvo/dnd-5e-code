import json
from typing import Union, List, Any, Optional

import requests

from dnd.Entity.Equipment import Equipment
from dnd.Loaders.ILoader import ILoader
from dnd.Loaders.api.ApiSettings import ApiSettings


class EquipmentApiLoader:
    def __init__(self, settings: ApiSettings):
        self.settings = settings
        self.url = settings.url + '/api/equipment'

    def load(self) -> List[Equipment]:
        response = requests.get(self.url)
        data = response.json()
        result: List[Equipment] = []
        for e in data:
            eq = Equipment(**e)
            result.append(eq)
        return result

    def fetch(self, identifier: Any) -> Optional[Equipment]:
        pass

    def save(self, items: Union[List[Equipment], Equipment]):
        if not isinstance(items, List):
            items = [items]
        for item in items:
            self.save_one(item)

    def save_one(self, item: Equipment):
        if item is None:
            return
        params = item.__dict__.copy()
        dto = Equipment(**params)
        d = dto.__dict__
        d.pop('id')
        data = json.dumps(d)
        url = self.settings.url + '/api/equipment'
        response = requests.put(url=url + f'/{item.id}', data=data)
        assert response.status_code == 200
