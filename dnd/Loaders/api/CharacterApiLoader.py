import json
from typing import Union, List, Any, Optional

import requests

from dnd.Entity.Character import Character
from dnd.Loaders.ILoader import ILoader
from dnd.Loaders.api.ApiSettings import ApiSettings


class CharacterApiLoader:
    def __init__(self, settings: ApiSettings):
        self.settings = settings
        self.url = settings.url + '/api/character'

    @staticmethod
    def _populate_single(e: dict) -> Character:
        return Character(**e)

    def load(self) -> List[Character]:
        response = requests.get(self.url)
        data = response.json()
        result: List[Character] = []
        for e in data:
            result.append(self._populate_single(e))
        return result

    def fetch(self, identifier: Any) -> Optional[Character]:
        response = requests.get(self.url + '/' + str(identifier))
        data = response.json()
        return self._populate_single(data)

    def save(self, items: Union[List[Character], Character]):
        if not isinstance(items, List):
            items = [items]
        for item in items:
            self.save_one(item)

    def save_one(self, item: Character):
        if item is None:
            return
        params = item.__dict__.copy()
        dto = Character(**params)
        d = dto.__dict__
        d.pop('id')
        data = json.dumps(d)
        url = self.settings.url + '/api/character'
        response = requests.put(url=url + f'/{item.id}', data=data)
        assert response.status_code == 200
