import json
from typing import Union, List, Any, Optional

import requests

from dnd.Entity.Feature import Feature
from dnd.Loaders.api.ApiSettings import ApiSettings


class FeatureApiLoader:
    def __init__(self, settings: ApiSettings):
        self.settings = settings
        self.url = settings.url + '/api/feature'

    def load(self) -> List[Feature]:
        response = requests.get(self.url)
        data = response.json()
        result: List[Feature] = []
        for e in data:
            eq = Feature(**e)
            result.append(eq)
        return result

    def fetch(self, identifier: Any) -> Optional[Feature]:
        pass

    def save(self, items: Union[List[Feature], Feature]):
        if not isinstance(items, List):
            items = [items]
        for item in items:
            self.save_one(item)

    def save_one(self, item: Feature):
        if item is None:
            return
        params = item.__dict__.copy()
        dto = Feature(**params)
        d = dto.__dict__
        d['class_requirement'] = '|'.join(d['class_requirement'])
        d['prerequisites'] = '|'.join(d['prerequisites'])
        if isinstance(d['description'], list):
            d['description'] = '\n'.join(d['description'])
        d.pop('id')
        data = json.dumps(d)
        url = self.url
        response = requests.put(url=url + f'/{item.id}', data=data)
        assert response.status_code == 200
