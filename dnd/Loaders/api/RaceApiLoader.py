from typing import Union, List, Any, Optional

import requests

from dnd.Entity.Race import Race
from dnd.Loaders.ILoader import ILoader, T


class RaceApiLoader (ILoader[Race]):
    def load(self) -> List[T]:
        data = requests.get('https://www.dnd5eapi.co/api/equipment-categories/adventuring-gear')
        response: List[Race] = []
        for e in data['equipment']:
            eq = Race(name=e['name'])
            response.append(eq)
        return response

    def fetch(self, identifier: Any) -> Optional[Race]:
        pass

    def save(self, items: Union[List[Race], Race]):
        pass

    def save_one(self, item: Race):
        pass

