from abc import ABC, abstractmethod
from typing import Optional, Any, Iterator, Tuple

from dnd.Entity.Character import Character
from dnd.Entity.Choices.Choice import Choice
from dnd.Entity.Class import Class
from dnd.Entity.Race import Race
from dnd.Entity.Stat import Stat


class IPlayerCharacter(ABC):
    @abstractmethod
    def get_entities(self) -> Tuple[Character, Class, Race]:
        pass

    @abstractmethod
    def ask(self, entity: Any) -> Iterator[Choice]:
        pass

    @abstractmethod
    def setup_character(self) -> Character:
        pass

    @abstractmethod
    def load(self, name: str):
        pass

    @abstractmethod
    def save(self, character: Optional[Character] = None):
        pass

    @abstractmethod
    def get_stats(self) -> Stat:
        pass

    @abstractmethod
    def get_proficiency(self) -> int:
        pass
