from typing import Generic, TypeVar, List, Any, Optional, Union
from abc import ABC, abstractmethod

T = TypeVar("T")
K = TypeVar("K")


class ILoader(Generic[T], ABC):
    @abstractmethod
    def load(self) -> List[T]:
        pass

    @abstractmethod
    def fetch(self, identifier: Any) -> Optional[T]:
        pass

    @abstractmethod
    def save(self, items: Union[List[T], T]):
        pass

    @abstractmethod
    def save_one(self, item: T):
        pass
