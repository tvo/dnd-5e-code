import pathlib
from functools import lru_cache
from os.path import exists
from dataclasses import dataclass
from typing import TypeVar, Generic, List, Any, Optional, Callable, Union

from easy_inject import ServiceCollection

from dnd.Entity.Character import Character
from dnd.Entity.Class import Class
from dnd.Entity.Equipment import Equipment
from dnd.Entity.Feature import Feature
from dnd.Entity.Race import Race
from dnd.Entity.Weapon import Weapon
from dnd.Loaders.ILoader import ILoader
from dnd.Loaders.ILoaderCollection import ILoaderCollection
from dnd.Loaders.JsonConvert import serialize, deserialize
from dnd.Loaders.api.EquipmentApiLoader import EquipmentApiLoader
from dnd.Loaders.srd_api.EquipmentSrdLoader import EquipmentSrdLoader

T = TypeVar("T")
K = TypeVar("K")


@dataclass
class Container(Generic[T]):
    items: List[T]

    def index_of_property_identifier(self, identifier: K, key_func: Callable[[T], K]) -> Optional[int]:
        return next((i for i, v in enumerate(self.items) if identifier == key_func(v)), None)

    def index_of_property(self, item: T, key_func: Callable[[T], K]) -> Optional[int]:
        item_id: K = key_func(item)
        return self.index_of_property_identifier(item_id, key_func)

    @staticmethod
    def _get_key(item: Any) -> Optional[Any]:
        if item.name is not None:
            return item.name
        elif item.id is not None:
            return item.id
        return None

    def fetch_index_identifier(self, identifier: K) -> Optional[int]:
        return self.index_of_property_identifier(identifier, lambda i: self._get_key(i))

    def fetch_index(self, item: T) -> Optional[int]:
        return self.index_of_property(item, lambda i: self._get_key(i))

    def insert(self, item: T, position: int):
        if position is None or position == -1:
            self.items.append(item)
        else:
            self.items[position] = item

    def merge(self, item: T):
        if item is None:
            return
        index = self.fetch_index(item)
        self.insert(item, index)


class BaseLoader(Generic[T], ILoader[T]):
    def __init__(self, path: str):
        self.path = pathlib.Path().resolve().__str__() + '/data/' + path

    @lru_cache()
    def load(self) -> List[T]:
        if not exists(self.path):
            return []
        with open(self.path, 'r') as f:
            data = f.read()
            return deserialize(raw=data)

    def fetch(self, identifier: Any) -> Optional[T]:
        existing: Container[T] = Container[T](items=self.load())
        index = existing.fetch_index_identifier(identifier)
        if index is None:
            return None
        return existing.items[index]

    def save(self, items: Union[List[T], T]):
        if getattr(self.load, 'cache_clear'):
            # noinspection PyUnresolvedReferences
            self.load.cache_clear()
        if items is not List:
            items = [items]
        existing: Container[T] = Container[T](items=self.load())
        for item in items:
            existing.merge(item)
        data = serialize(existing.items)
        with open(self.path, 'w') as f:
            f.write(data)

    def save_one(self, item: T):
        self.save([item])


class WeaponLoader(BaseLoader[Weapon]):
    def __init__(self):
        super().__init__('weapon.json')


class ClassLoader(BaseLoader[Class]):
    def __init__(self):
        super().__init__('class.json')


class EquipmentLoader(BaseLoader[Equipment]):
    def __init__(self):
        super().__init__('equipment.json')


class RaceLoader(BaseLoader[Race]):
    def __init__(self):
        super().__init__('races.json')


class CharacterLoader(BaseLoader[Character]):
    def __init__(self):
        super().__init__('characters.json')


class FeatureLoader(BaseLoader[Feature]):
    def __init__(self):
        super().__init__('features.json')


@dataclass
class Loaders (ILoaderCollection):
    weapon_loader: WeaponLoader
    class_loader: ClassLoader
    equipment_loader: EquipmentLoader
    race_loader: RaceLoader
    character_loader: CharacterLoader
    feature_loader: FeatureLoader

    def get_weapon(self) -> ILoader[Weapon]:
        return self.weapon_loader

    def get_equipment(self) -> ILoader[Equipment]:
        return self.equipment_loader

    def get_class(self) -> ILoader[Class]:
        return self.class_loader

    def get_race(self) -> ILoader[Race]:
        return self.race_loader

    def get_character(self) -> ILoader[Character]:
        return self.character_loader

    def get_feature(self) -> ILoader[Feature]:
        return self.feature_loader

    @staticmethod
    def dependencies(services: ServiceCollection):
        services.register(WeaponLoader).singleton()
        services.register(RaceLoader).singleton()
        services.register(ClassLoader).singleton()
        services.register(EquipmentLoader).singleton()
        services.register(CharacterLoader).singleton()
        services.register(FeatureLoader).singleton()
        services.register(Loaders).singleton()
