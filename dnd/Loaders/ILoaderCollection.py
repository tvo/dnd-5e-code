from typing import Generic, TypeVar, List, Any, Optional, Union
from abc import ABC, abstractmethod

from dnd.Entity.Character import Character
from dnd.Entity.Class import Class
from dnd.Entity.Equipment import Equipment
from dnd.Entity.Feature import Feature
from dnd.Entity.Race import Race
from dnd.Entity.Weapon import Weapon
from dnd.Loaders.ILoader import ILoader


class ILoaderCollection(ABC):
    @abstractmethod
    def get_weapon(self) -> ILoader[Weapon]:
        pass

    @abstractmethod
    def get_equipment(self) -> ILoader[Equipment]:
        pass

    @abstractmethod
    def get_class(self) -> ILoader[Class]:
        pass

    @abstractmethod
    def get_race(self) -> ILoader[Race]:
        pass

    @abstractmethod
    def get_character(self) -> ILoader[Character]:
        pass

    @abstractmethod
    def get_feature(self) -> ILoader[Feature]:
        pass
