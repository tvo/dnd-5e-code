from typing import Optional, Union

from d20 import RollSyntaxError

from dnd.Entity.DiceRoll import DiceRoll


def get_str(item: dict, key: str):
    keys = key.split('.')
    k = keys[0]
    if k not in item.keys():
        return None
    item = item[k]
    if len(keys) > 1:
        return get_str(item, '.'.join(keys[1:]))
    if isinstance(item, list):
        return '\n'.join(item)
    return item


def parse_integer(value: Optional[Union[str, float, int]], default: int = 0) -> int:
    value = value or default
    if isinstance(value, int):
        return value
    if isinstance(value, float):
        return int(value)
    return int(value) if value.isdigit() else default


def parse_float(value: Optional[Union[str, int, float]], default: float = 0) -> float:
    value = value or 0.0
    if isinstance(value, int):
        return value * 1.0
    if isinstance(value, float):
        return value
    return float(value) if value.isdecimal() else default


def parse_damage(item) -> DiceRoll:
    try:
        return DiceRoll.parse(get_str(item, 'damage.damage_dice'))
    except RollSyntaxError:
        return DiceRoll(dice=[])


moneyValue = dict()
moneyValue['pl'] = 10
moneyValue['gp'] = 1
moneyValue['sp'] = 0.1
moneyValue['cp'] = 0.01


def parse_cost(item):
    unit = get_str(item, 'cost.unit') or 'gp'
    quantity = parse_float(get_str(item, 'cost.quantity'), 0.0)
    return quantity * moneyValue[unit]
