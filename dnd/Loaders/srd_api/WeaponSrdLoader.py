from typing import Union, List, Any, Optional

import requests

from dnd.Entity.Dice import Dice
from dnd.Entity.DiceRoll import DiceRoll
from dnd.Entity.Weapon import Weapon
from dnd.Loaders.ILoader import ILoader, T
from dnd.Loaders.srd_api.Common import parse_cost, parse_damage, parse_integer, get_str


def _has_property(item: dict, value) -> bool:
    if 'properties' not in item.keys():
        return False
    for p in item['properties']:
        if p['index'] == value:
            return True
    return False


class WeaponSrdLoader (ILoader[Weapon]):
    def load(self) -> List[Weapon]:
        response = requests.get('https://www.dnd5eapi.co/api/equipment-categories/weapon')
        data = response.json()
        result: List[Weapon] = []
        unset_damage = DiceRoll(dice=[Dice(sides=1)], plus=0)
        for e in data['equipment']:
            eq = Weapon(id=e['index'], name=e['name'], description='', value=0, weight=0, damage_roll=unset_damage)
            result.append(eq)
        return result

    def fetch(self, identifier: str) -> Optional[T]:
        item = self._fetch(identifier)
        if item is not None:
            return item
        return self._fetch(identifier, magic=True)

    @staticmethod
    def _fetch(identifier: str, magic: bool = False) -> Optional[T]:
        identifier = identifier.lower()
        identifier = identifier.replace(' ', '-')
        identifier = identifier.replace(',', '')
        category = 'equipment'
        if magic:
            category = 'magic-items'
        response = requests.get(f'https://www.dnd5eapi.co/api/{category}/{identifier}')
        data = response.json()
        if 'error' in data.keys():
            return None
        result: Weapon = Weapon(
            id=data['index'],
            name=data['name'],
            description=get_str(data, 'desc') or '',
            value=parse_cost(data),
            damage_roll=parse_damage(data),
            weight=parse_integer(get_str(data, 'weight')),
            range=parse_integer(get_str(data, 'range.normal')),
            category=get_str(data, 'weapon_category') or ''
        )
        result.finesse = _has_property(data, 'finesse')
        result.two_handed = _has_property(data, 'two_handed')
        result.thrown = _has_property(data, 'thrown')
        result.light = _has_property(data, 'light')
        result.heavy = _has_property(data, 'heavy')
        result.loading = _has_property(data, 'loading')
        result.versatile = _has_property(data, 'versatile')
        result.thrown = _has_property(data, 'thrown')
        result.reach = _has_property(data, 'reach')

        return result

    def save(self, items: Union[List[T], T]):
        pass

    def save_one(self, item: T):
        pass
