from typing import Union, List, Optional

import requests

from dnd.Entity.Feature import Feature
from dnd.Loaders.ILoader import ILoader


class FeatureSrdLoader (ILoader[Feature]):
    def load(self) -> List[Feature]:
        response = requests.get('https://www.dnd5eapi.co/api/features')
        data = response.json()
        result: List[Feature] = []
        for e in data['results']:
            eq = Feature(id=e['index'], name=e['name'], description='')
            result.append(eq)
        return result

    def fetch(self, identifier: str) -> Optional[Feature]:
        identifier = identifier.lower()
        identifier = identifier.replace(' ', '-').replace(',', '').replace('(', '').replace(')', '')
        identifier = identifier
        response = requests.get(f'https://www.dnd5eapi.co/api/features/{identifier}')
        data = response.json()
        if 'error' in data.keys():
            return None
        result: Feature = Feature(
            id=data['index'],
            name=data['name'],
            description=data['desc'],
            level=int(data['level']),
        )
        if data['class']:
            result.class_requirement.append(data['class']['name'])

        return result

    def save(self, items: Union[List[Feature], Feature]):
        pass

    def save_one(self, item: Feature):
        pass
