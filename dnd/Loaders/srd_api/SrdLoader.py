from dataclasses import dataclass

from easy_inject import ServiceCollection

from dnd.Entity.Character import Character
from dnd.Entity.Class import Class
from dnd.Entity.Equipment import Equipment
from dnd.Entity.Feature import Feature
from dnd.Entity.Race import Race
from dnd.Entity.Weapon import Weapon
from dnd.Loaders.ILoader import ILoader
from dnd.Loaders.ILoaderCollection import ILoaderCollection
from dnd.Loaders.srd_api.ClassSrdLoader import ClassSrdLoader
from dnd.Loaders.srd_api.EquipmentSrdLoader import EquipmentSrdLoader
from dnd.Loaders.srd_api.FeatureSrdLoader import FeatureSrdLoader
from dnd.Loaders.srd_api.RaceSrdLoader import RaceSrdLoader
from dnd.Loaders.srd_api.WeaponSrdLoader import WeaponSrdLoader


@dataclass
class SrdLoaders (ILoaderCollection):
    weapon_loader: WeaponSrdLoader
    class_loader: ClassSrdLoader
    equipment_loader: EquipmentSrdLoader
    race_loader: RaceSrdLoader
    feature_loader: FeatureSrdLoader

    def get_weapon(self) -> ILoader[Weapon]:
        return self.weapon_loader

    def get_equipment(self) -> ILoader[Equipment]:
        return self.equipment_loader

    def get_class(self) -> ILoader[Class]:
        return self.class_loader

    def get_race(self) -> ILoader[Race]:
        return self.race_loader

    def get_character(self) -> ILoader[Character]:
        pass

    def get_feature(self) -> ILoader[Feature]:
        return self.feature_loader

    @staticmethod
    def dependencies(services: ServiceCollection):
        services.register(WeaponSrdLoader).singleton()
        services.register(RaceSrdLoader).singleton()
        services.register(ClassSrdLoader).singleton()
        services.register(EquipmentSrdLoader).singleton()
        services.register(FeatureSrdLoader).singleton()
        services.register(SrdLoaders).singleton()
