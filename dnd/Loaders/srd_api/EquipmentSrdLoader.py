from typing import Union, List, Any, Optional

import requests

from dnd.Entity.Equipment import Equipment
from dnd.Loaders.ILoader import ILoader, T
from dnd.Loaders.srd_api.Common import parse_cost, get_str, parse_integer


class EquipmentSrdLoader (ILoader[Equipment]):
    def load(self) -> List[Equipment]:
        response = requests.get('https://www.dnd5eapi.co/api/equipment-categories/adventuring-gear')
        data = response.json()
        result: List[Equipment] = []
        for e in data['equipment']:
            eq = Equipment(id=e['index'], name=e['name'], description='', value=0, weight=0)
            result.append(eq)
        return result

    def fetch(self, identifier: str) -> Optional[Equipment]:
        identifier = identifier.lower()
        identifier = identifier.replace(' ', '-')
        identifier = identifier.replace(',', '')
        response = requests.get(f'https://www.dnd5eapi.co/api/equipment/{identifier}')
        data = response.json()
        if 'error' in data.keys():
            return None
        result: Equipment = Equipment(
            id=data['index'],
            name=data['name'],
            description=get_str(data, 'desc') or '',
            value=parse_cost(data),
            weight=parse_integer(get_str(data, 'weight')),
        )
        return result

    def save(self, items: Union[List[Equipment], Equipment]):
        pass

    def save_one(self, item: Equipment):
        pass

