from dataclasses import dataclass

from easy_inject import ServiceCollection

from dnd.Entity.Character import Character
from dnd.Entity.Class import Class
from dnd.Entity.Equipment import Equipment
from dnd.Entity.Feature import Feature
from dnd.Entity.Race import Race
from dnd.Entity.Weapon import Weapon
from dnd.Loaders.ILoader import ILoader
from dnd.Loaders.ILoaderCollection import ILoaderCollection
from dnd.Loaders.beyond.BeyondChoiceParser import BeyondChoiceParser
from dnd.Loaders.beyond.CharacterBeyondLoader import CharacterBeyondLoader


@dataclass
class BeyondLoaders (ILoaderCollection):
    character_loader: CharacterBeyondLoader

    def get_weapon(self) -> ILoader[Weapon]:
        pass

    def get_equipment(self) -> ILoader[Equipment]:
        pass

    def get_class(self) -> ILoader[Class]:
        pass

    def get_race(self) -> ILoader[Race]:
        pass

    def get_character(self) -> ILoader[Character]:
        return self.character_loader

    def get_feature(self) -> ILoader[Feature]:
        pass

    @staticmethod
    def dependencies(services: ServiceCollection):
        services.register(BeyondChoiceParser)
        # loaders
        services.register(CharacterBeyondLoader).singleton()
        services.register(BeyondLoaders).singleton()

