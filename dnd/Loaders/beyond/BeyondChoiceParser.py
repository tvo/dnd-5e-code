from typing import List, Iterator, Any, Optional

from dnd.Entity.Choices.Choice import Choice
from dnd.Entity.Choices.LevelUpChoice import LevelUpChoice
from dnd.Entity.Choices.SkillChoice import SkillChoice
from dnd.Entity.Stat import Stat


def _enumerate_with_key(data: Any, key: str) -> Iterator[dict]:
    if isinstance(data, dict):
        if key in data.keys():
            yield data
        for (k, d) in data.items():
            for other in _enumerate_with_key(d, key):
                yield other
    if isinstance(data, list):
        for d in data:
            for c in _enumerate_with_key(d, key):
                yield c


class BeyondChoiceParser:
    def __init__(self):
        self.id_dict: dict = {}
        self.component_dict: dict = {}

    def _get_option_value(self, skill_component: dict) -> Any:
        answer_key = skill_component.get('optionValue') or {}
        answer_item = self.id_dict.get(answer_key) or self.component_dict.get(answer_key) or {}
        answer_label = answer_item.get('label') or answer_item.get('name')
        return answer_label

    def _parse_skill_choice(self, skill_component, id_dict: dict) -> Optional[SkillChoice]:
        c = SkillChoice()
        if 'optionValue' not in skill_component.keys():
            return None
        c.answer = self._get_option_value(skill_component)
        if c.answer is None:
            return None
        for opt in skill_component['optionIds']:
            c.choices.append(id_dict[opt]['label'])
        return c

    def _parse_feat(self, skill_component: dict) -> Optional[LevelUpChoice]:
        c = LevelUpChoice()
        if 'optionValue' not in skill_component.keys():
            return None
        answer_label = self._get_option_value(skill_component)
        c.feature_pick = answer_label
        return c

    def _parse_ability_choice(self, skill_component: dict) -> Optional[LevelUpChoice]:
        c = LevelUpChoice()
        if 'optionValue' not in skill_component.keys():
            return None
        answer_label = self._get_option_value(skill_component)
        amount = 1
        if 'by 2' in skill_component.get('label'):
            amount = 2
        c.stat_increase = Stat.get_from_lose_label(answer_label, amount)
        return c

    def _parse_choice_item(self, c_data) -> List[Choice]:
        if c_data is None or 'label' not in c_data.keys() or c_data['label'] is None:
            return []
        if 'skill' in c_data['label'] or 'Skill' in c_data['label']:
            return [self._parse_skill_choice(c_data, self.id_dict)]
        if 'Ability' in c_data['label']:
            return [self._parse_ability_choice(c_data)]
        if 'Choose a Feat' in c_data['label']:
            return [self._parse_feat(c_data)]
        return []

    def _get_choices(self, choice_data: dict, category: str) -> List[Choice]:
        c = []
        for c_data in choice_data[category]:
            c.extend(self._parse_choice_item(c_data))
        return c

    @staticmethod
    def _dict_search_via_key(data: dict, key: str) -> dict:
        result = list(_enumerate_with_key(data, key))
        return {result[i][key]: result[i] for i in range(0, len(result))}

    def parse_choices(self, data: dict) -> List[Choice]:
        self.id_dict = self._dict_search_via_key(data, 'id')
        self.component_dict = self._dict_search_via_key(data, 'componentId')

        choices: List[Choice] = []
        choice_data = data['choices']
        choices.extend(self._get_choices(choice_data, 'class'))
        choices.extend(self._get_choices(choice_data, 'race'))
        choices.extend(self._get_choices(choice_data, 'background'))

        return choices
