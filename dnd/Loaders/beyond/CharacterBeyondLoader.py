import pathlib
from typing import List, Any, Optional, Union

import diskcache
import requests

from dnd.Entity.Character import Character
from dnd.Entity.Stat import Stat
from dnd.Loaders.beyond.BeyondChoiceParser import BeyondChoiceParser
from dnd.Loaders.srd_api.Common import get_str

cache_path = pathlib.Path().resolve().__str__() + '/data/cache'
cache = diskcache.Cache(cache_path)


def _parse_stat_block(stat_block: dict) -> Stat:
    # dnd beyond convention: str dex con int wis cha.
    mapping = [2, 1, 0, 3, 4, 5]
    r_list = list(map(lambda key_i: stat_block[key_i]['value'], mapping))
    return Stat.from_array(r_list)


class CharacterBeyondLoader:
    def __init__(self, c_parser: BeyondChoiceParser):
        self.choice_parser = c_parser

    @cache.memoize()
    def _download_json_character_id(self, id: str):
        url = f'https://character-service.dndbeyond.com/character/v5/character/{id}?includeCustomItems=true'
        data = requests.get(url).json()
        return data

    def fetch(self, identifier: Any) -> Optional[Character]:
        data = self._download_json_character_id(identifier)['data']
        r = Character()
        # r.level = parse_integer(get_str(data, ''))
        # r.cl = get_str(data['class'][0], 'definition.name')
        r.race_name = get_str(data, 'race.baseRaceName')
        r.base_stats = _parse_stat_block(data['stats'])
        r.choices = self.choice_parser.parse_choices(data)
        return r

    def load(self) -> List[Character]:
        pass

    def save(self, items: Union[List[Character], Character]):
        pass

    def save_one(self, item: Character):
        pass
