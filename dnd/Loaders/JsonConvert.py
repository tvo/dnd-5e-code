from typing import Any, TypeVar, Type
from json_tricks import dumps, loads

T = TypeVar("T")


def serialize(obj: Any) -> str:
    return dumps(obj)


def deserialize(raw: str) -> T:
    return loads(raw)
