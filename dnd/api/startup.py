from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise

from dnd.api import DAL
from dnd.api.crud import crud_endpoint, modules
import uvicorn

TORTOISE_ORM = {
    'connections': {'default': 'sqlite://db.sqlite'},
    'apps': {
        'models': {
            'models': ['aerich.models'],
            'default_connection': 'default',
        },
    },
}
models = [
    ['language', DAL.Language],
    ['equipment', DAL.Equipment],
    ['weapon', DAL.Weapon],
    ['feature', DAL.Feature],
]
for m in models:
    TORTOISE_ORM['apps']['models']['models'].append('dnd.api.DAL.' + m[1].__name__)


def run():
    app = FastAPI()
    for m in models:
        crud_endpoint(name=m[0], app=app, db_model=m[1]).register_routes()

    modules.append('aerich.models')
    # db_url = 'sqlite://:memory:',
    register_tortoise(
        app,
        db_url='sqlite://db.sqlite',
        modules={'models': modules},
        generate_schemas=True,
        add_exception_handlers=True,
    )

    uvicorn.run(app=app, host="127.0.0.1", port=5000, log_level="info")
    print('done')
