from tortoise.models import Model
from tortoise import fields


class Equipment(Model):
    id = fields.TextField(pk=True)
    name = fields.TextField()
    description = fields.TextField()
    value = fields.IntField()
    weight = fields.IntField()
