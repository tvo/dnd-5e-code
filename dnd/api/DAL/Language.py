from tortoise.models import Model
from tortoise import fields


class Language(Model):
    id = fields.TextField(pk=True)
    name = fields.TextField()

    def __str__(self):
        return self.name
