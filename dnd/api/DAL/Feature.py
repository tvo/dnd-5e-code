from tortoise.models import Model
from tortoise import fields


class Feature(Model):
    id: str = fields.TextField(pk=True)
    name: str = fields.TextField()
    description: str = fields.TextField()
    class_requirement: str = fields.TextField()
    prerequisites: str = fields.TextField()
    level: int = fields.IntField()
