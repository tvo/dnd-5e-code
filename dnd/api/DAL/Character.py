from tortoise.models import Model
from tortoise import fields


class Character(Model):
    id: str = fields.TextField(pk=True)
    name: str = fields.TextField()
    race_name: str = fields.TextField()
    baseStats: str = fields.TextField()  # format will be 10,10,10,10,10,10

    # mainClass: CharacterClass
    # subClass: Optional[CharacterClass] = field(default_factory=lambda: [])
    # multiClasses: List[CharacterClass] = field(default_factory=lambda: [])
    # choices: List[Choice] = field(default_factory=lambda: [])
