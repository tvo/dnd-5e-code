from tortoise.models import Model
from tortoise import fields


class Weapon(Model):
    id: str = fields.TextField(pk=True)
    name: str = fields.TextField()
    description: str = fields.TextField()
    value: float = fields.FloatField()
    weight: float = fields.FloatField()
    damage_roll: str = fields.TextField()
    to_hit_bonus: int = fields.IntField()
    range: int = fields.IntField()
    light: bool = fields.BooleanField()
    two_handed: bool = fields.BooleanField()
    finesse: bool = fields.BooleanField()
    loading: bool = fields.BooleanField()
    versatile: bool = fields.BooleanField()
    thrown: bool = fields.BooleanField()
    reach: bool = fields.BooleanField()
    heavy: bool = fields.BooleanField()
    category: str = fields.TextField()
