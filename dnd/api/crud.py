from typing import TypeVar, Type, Generic, List
import tortoise
from fastapi.applications import FastAPI
from tortoise.contrib.pydantic import pydantic_model_creator

TDB = TypeVar('TDB', bound=tortoise.Model)

modules = []


def variable_names(model) -> List[str]:
    return [element for element in filter(lambda e: '__' not in e and not e.startswith('_'), vars(model))]


def update_model(f, t):
    from_fields = variable_names(f)
    to_fields = variable_names(t)
    for field in from_fields:
        if field in to_fields:
            value = getattr(f, field)
            setattr(t, field, value)


class crud_endpoint(Generic[TDB]):
    def __init__(self, name: str, app: FastAPI, db_model: Type[TDB]):
        modules.append(db_model.__module__)
        self.name = name
        self.app = app
        self.db_model = db_model

    def register_routes(self):
        model_pydantic = pydantic_model_creator(self.db_model, name=f"model_{self.name}")
        in_pydantic = pydantic_model_creator(self.db_model, name=f"in_{self.name}", exclude_readonly=True)

        @self.app.get(f'/api/{self.name}')
        async def get():
            return await model_pydantic.from_queryset(self.db_model.all())

        @self.app.put(f'/api/{self.name}/' + '{id_url}', response_model=model_pydantic)
        async def put(id_url: str, model: in_pydantic):
            to_update = await self.db_model.get_or_none(id=id_url)
            if to_update is not None:
                await self.db_model.filter(id=id_url).update(**model.dict())
            else:
                await self.db_model.create(id=id_url, **model.dict(exclude_unset=True))
            return await model_pydantic.from_queryset_single(self.db_model.get(id=id_url))
