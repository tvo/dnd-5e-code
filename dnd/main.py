from time import sleep
from typing import Any, List

from easy_inject import ServiceCollection
from easy_inject.Scope import IScope

from dnd.Calculators.Calculators import Calculators
from dnd.Entity.Class import Class
from dnd.Entity.Dice import Dice
from dnd.Entity.DiceRoll import DiceRoll
from dnd.Entity.Stat import Stat
from dnd.Loaders.ILoader import ILoader
from dnd.Loaders.ILoaderCollection import ILoaderCollection
from dnd.Loaders.IPlayerCharacter import IPlayerCharacter
from dnd.Loaders.Loaders import ClassLoader, Loaders
from dnd.Loaders.api.ApiLoader import ApiLoaders
from dnd.Loaders.beyond.BeyondLoaders import BeyondLoaders
from dnd.Loaders.srd_api.SrdLoader import SrdLoaders
from dnd.PlayerCharacter import PlayerCharacter


def build_service_collection() -> ServiceCollection:
    collection = ServiceCollection()
    Loaders.dependencies(collection)
    SrdLoaders.dependencies(collection)
    ApiLoaders.dependencies(collection)
    BeyondLoaders.dependencies(collection)
    Calculators.dependencies(collection)

    collection.register(PlayerCharacter).As(PlayerCharacter).As(IPlayerCharacter)
    return collection


def player_setup(scope: IScope) -> PlayerCharacter:
    player = scope.resolve(PlayerCharacter)
    player.load(name='human')

    # if not found, this promps the user for input.
    player.setup_character()

    player.character.mainClass.level = 2
    player.character.base_stats = Stat.from_array([15, 9, 16, 11, 15, 14])

    player.initialize_character_properties()
    print(player.max_hp)
    print(player.character.mainClass.level)
    player.save()
    return player


def race_stuff(scope: ServiceCollection):
    hit_dice = DiceRoll(dice=[Dice(sides=10)], plus=0)
    hit_start = DiceRoll(dice=[], plus=10)
    instance = Class(name='ranger', hit_dice=hit_dice, hit_start=hit_start, spell_casting_ability="wisdom")
    instance.saving_throw_proficiencies.strength = 1
    instance.saving_throw_proficiencies.dexterity = 1
    loader = scope.resolve(ClassLoader)
    loader.save(instance)


def load_api_specific(srd: ILoader[Any], api: ILoader[Any], reload: bool = True):
    items: List = api.load()
    srd_items: List = srd.load()
    api_ids = list(map(lambda x: x.id, items))
    if not reload and len(items) == len(srd_items):
        return
    for item in srd_items:
        if not reload and item.id in api_ids:
            continue
        better_item = srd.fetch(item.id)
        api.save(better_item)
        sleep(1)
    print(items)


def load_api_stuff(scope: IScope):
    srd_loader = scope.resolve(SrdLoaders)
    api_loader = scope.resolve(ApiLoaders)

    load_api_specific(srd_loader.get_weapon(), api_loader.get_weapon())
    # load_api_specific(srd_loader.get_equipment(), api_loader.get_equipment())
    # load_api_specific(srd_loader.get_feature(), api_loader.get_feature())

# https://character-service.dndbeyond.com/character/v5/character/66892962?includeCustomItems=true


def weapon_expected_value(scope: IScope):
    api_loader = scope.resolve(ApiLoaders)
    weapons = api_loader.get_weapon().load()
    weapon_decorator = scope.resolve(Calculators).weapon_character

    player = player_setup(scope)
    player.stats = Stat(16, 8, 16, 16, 16, 16)

    print_list = []
    for w in weapons:
        w = weapon_decorator.decorate_weapon(w, player)
        dmg = w.damage_expected_value(player.get_stats())
        print_list.append([w.name, dmg, w.to_hit_bonus])
    print_list.sort(key=lambda y: y[1])
    for items in print_list:
        print(items)


def beyond_loader_stuff(scope: IScope):
    b_loader = scope.resolve(BeyondLoaders)
    c_loader = b_loader.get_character()
    monty = c_loader.fetch(66892962)
    scope.resolve(ApiLoaders).get_character().save(monty)


def run():
    sp = build_service_collection()
    with sp.createScope() as scope:
        # load_api_stuff(scope)
        # race_stuff(scope)
        # player_setup(scope)
        # weapon_expected_value(scope)
        beyond_loader_stuff(scope)
    print('done')

