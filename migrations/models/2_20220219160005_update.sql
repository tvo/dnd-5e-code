-- upgrade --
ALTER TABLE "weapon" ADD "category" TEXT NOT NULL DEFAULT('');
-- downgrade --
ALTER TABLE "weapon" DROP COLUMN "category";
