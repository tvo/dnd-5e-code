-- upgrade --
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" JSON NOT NULL
);
CREATE TABLE IF NOT EXISTS "language" (
    "id" TEXT NOT NULL  PRIMARY KEY,
    "name" TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "equipment" (
    "id" TEXT NOT NULL  PRIMARY KEY,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "value" INT NOT NULL,
    "weight" INT NOT NULL
);
CREATE TABLE IF NOT EXISTS "weapon" (
    "id" TEXT NOT NULL  PRIMARY KEY,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "value" REAL NOT NULL,
    "weight" REAL NOT NULL,
    "damage_roll" TEXT NOT NULL,
    "to_hit_bonus" INT NOT NULL,
    "range" INT NOT NULL,
    "light" INT NOT NULL,
    "two_handed" INT NOT NULL,
    "finesse" INT NOT NULL,
    "loading" INT NOT NULL,
    "versatile" INT NOT NULL,
    "thrown" INT NOT NULL,
    "reach" INT NOT NULL,
    "heavy" INT NOT NULL,
);
CREATE TABLE IF NOT EXISTS "feature" (
    "id" TEXT NOT NULL  PRIMARY KEY,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "class_requirement" TEXT NOT NULL,
    "prerequisites" TEXT NOT NULL,
    "level" INT NOT NULL
);
