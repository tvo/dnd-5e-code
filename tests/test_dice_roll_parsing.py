import unittest

import pytest

from dnd.Entity.Dice import Dice
from dnd.Entity.DiceRoll import DiceRoll
from dnd.Entity.Race import Race
from dnd.Entity.Stat import Stat
from dnd.Loaders.JsonConvert import serialize, deserialize


class TestDiceParsing(unittest.TestCase):
    def test_dice_str(self):
        # Given
        dice = Dice(sides=2)
        # When
        raw = dice.__str__()
        # Then
        assert '1d2' == raw

    def test_dice_roll_OneTen(self):
        # Given
        roll = DiceRoll([Dice(sides=10)])
        # When
        raw = roll.__str__()
        # Then
        assert raw == '1d10'

    def test_dice_roll_OneTen_PlusFour(self):
        # Given
        roll = DiceRoll([Dice(sides=10)], plus=4)
        # When
        raw = roll.__str__()
        # Then
        assert raw == '1d10 + 4'

    def test_dice_roll_TwoTen(self):
        # Given
        roll = DiceRoll([Dice(sides=10), Dice(sides=10)])
        # When
        raw = roll.__str__()
        # Then
        assert raw == '2d10'

    def test_dice_roll_OneD2_Minus2(self):
        # Given
        roll = DiceRoll([Dice(sides=6)], plus=-2)
        # When
        raw = roll.__str__()
        # Then
        assert raw == '1d6 - 2'

    def test_dice_roll_OneD2_Twod10(self):
        # Given
        roll = DiceRoll([Dice(sides=6), Dice(sides=10), Dice(sides=10)])
        # When
        raw = roll.__str__()
        # Then
        assert raw == '1d6 + 2d10'

    def test_dice_roll_OneD2_Twod10_Plus100(self):
        # Given
        roll = DiceRoll([Dice(sides=6), Dice(sides=10), Dice(sides=10)], plus=100)
        # When
        raw = roll.__str__()
        # Then
        assert raw == '1d6 + 2d10 + 100'

    def test_dice_roll_parse_OneD6(self):
        # Given
        raw = '1d6'
        # When
        roll = DiceRoll.parse(raw)
        # Then
        assert roll.dice[0].sides == 6

    def test_dice_roll_parse_2D6(self):
        # Given
        raw = '2d6'
        # When
        roll = DiceRoll.parse(raw)
        # Then
        assert roll.dice[0].sides == 6
        assert roll.dice[1].sides == 6

    def test_dice_roll_parse_2D6Plus10(self):
        # Given
        raw = '2d6 + 10'
        # When
        roll = DiceRoll.parse(raw)
        # Then
        assert roll.dice[0].sides == 6
        assert roll.dice[1].sides == 6
        assert roll.plus == 10

    def test_dice_roll_parse_10plus1d6(self):
        # Given
        raw = '10 + 1d6'
        # When
        roll = DiceRoll.parse(raw)
        # Then
        assert roll.dice[0].sides == 6
        assert roll.plus == 10

    def test_dice_roll_parse_1d4_1d10(self):
        # Given
        raw = '1d4 + 1d10'
        # When
        roll = DiceRoll.parse(raw)
        # Then
        assert roll.dice[0].sides == 4
        assert roll.dice[1].sides == 10

    def test_dice_roll_parse_stat_values(self):
        # Given
        raw = '1d4 + con + str + dex + int + wis + cha'
        # When
        roll = DiceRoll.parse(raw)
        # Then
        assert roll.dice[0].sides == 4
        assert roll.plus == 0
        assert sum(roll.stat.to_array()) == 6

    def test_dice_roll_parse_stat_twice_values(self):
        # Given
        raw = '1d4'
        attrs = '+ con + str + dex + int + wis + cha'
        raw = raw + attrs + attrs
        # When
        roll = DiceRoll.parse(raw)
        # Then
        assert roll.dice[0].sides == 4
        assert roll.plus == 0
        assert sum(roll.stat.to_array()) == 12

    def test_dice_roll_with_stats_expected_value(self):
        # Given
        raw = '1d4 + con + str + dex + int + wis + cha'
        roll = DiceRoll.parse(raw)
        # When
        ev = roll.expected_value(Stat.from_array([20, 20, 20, 20, 20, 20]))
        # Then
        assert ev == 2.5 + (5 * 6)
