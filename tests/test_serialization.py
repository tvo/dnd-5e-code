import unittest

import pytest

from dnd.Entity.Dice import Dice
from dnd.Entity.DiceRoll import DiceRoll
from dnd.Entity.Race import Race
from dnd.Entity.Stat import Stat
from dnd.Loaders.JsonConvert import serialize, deserialize


class TestSerialization(unittest.TestCase):
    def test_serialization_dice(self):
        # Given
        dice = Dice(sides=2)
        # When
        raw = serialize(dice)
        # Then
        self.assertIn('2', raw)
        self.assertIn('sides', raw)

    def test_deserialization_dice(self):
        # Given
        raw = serialize(Dice(sides=2))
        # When
        dice = deserialize(raw)
        # Then
        self.assertEqual(dice.sides, 2)

    def test_serialization_dice_roll(self):
        entity = DiceRoll(dice=[Dice(2)], plus=5)
        raw = serialize(entity)
        output = deserialize(raw)
        self.assertEqual(entity.plus, output.plus)

    def test_serialization_stat(self):
        entity = Stat(1, 2, 3, 4, 5, 6)
        raw = serialize(entity)
        output = deserialize(raw)
        self.assertEqual(entity.wisdom, output.wisdom)

    def test_serialization_race(self):
        entity = Race('human')
        raw = serialize(entity)
        output = deserialize(raw)
        self.assertEqual(entity.name, output.name)
