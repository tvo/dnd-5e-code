import os
import setuptools
from os.path import exists
from setuptools import setup, find_packages


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(file_name):
    full_name = os.path.join(os.path.dirname(__file__), file_name)
    if not exists(full_name):
        return ''
    with open(full_name) as f:
        return f.read()


class ApiCommandStep(setuptools.Command):
    def run(self):
        print('asdfasdfasdf')


setup(
    name='dnd-code',
    description='code which explores some dnd concepts.',
    long_description=read('README.md'),
    install_requires=[],
    test_suite='tests',
    packages=find_packages(include=['dnd.*'], exclude=['tests', 'data']),
)
